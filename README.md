# CM Central Sensor System Nodes
Overview about the development for the central sensor system nodes.
Circuits, Documentations, Firmware will be available here for the nodes.

#### Information about the sensors for CM Central Sensor System
Following node type in planning or also developed

| Node type | Node discription | developed / in planning |
| -------- | -------- | -------- |
| sensorNode | typical sensor node, with many sensors on board. | developed |
| Alarmclock | A night table alarm clock, with many alarming times and possibility to a read a sd card for wav files. | development has started yet |
| WindowLevelSensor | for window handle a sensor that informate about the opened window and man more. | developed |
| CarAlarm | a car sensor to measure the situations in the car if they turned of and completed for the house door ;-). | development has not started yet |
| ShutterControl | a window shutter control sensor | development has not started yet |
| PowerStrip | a power control as a master slave power strip | development has started yes |
| RFID-DoorAlarm | a sensor control that is used to tell the CMCSS who is in the house (:-)) | developed |
----

#### SensorNode V1
![ View the first version from node sensor ](/pic/cssnodev1.png)

| Hardware | MAX1641, RFM12, HPO3s, HHOD1, LM75, Atmega328, DS18B20, LDR07 |  
| ------- | ------- |
----
#### SensorNode V2
<<< picture >>> 

| Hardware | MAX1641, RFM12, HPO3s, HHOD1, LM75, Atmega328, DS18B20, LDR07 |  
| ------- | ------- |




----
#### WindowLevelSensor V1
![ View the first version from Windows Level Sensor ](/pic/windowlevelsensorv1.png)

| Hardware | ZLDO330, MAX1811, RFM12, CNY72, HPO3s, HHOD1, LM75, Atmega324, LM324, LM358, LDR07, PIR-Sensor |
| ------- | ------- |
